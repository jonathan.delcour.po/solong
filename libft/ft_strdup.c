/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 23:49:43 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 09:13:29 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>
#include "libft.h"

char	*ft_strdup(const char *s)
{
	size_t	strlen;
	char	*result;

	strlen = ft_strlen(s);
	result = (char *)malloc(strlen + 1);
	if (!result)
		return (result);
	ft_strlcpy(result, s, strlen + 1);
	return (result);
}
