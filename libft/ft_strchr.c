/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 16:58:47 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/25 06:53:10 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strchr(const char *s, int c)
{
	char	c2;
	char	*str;

	str = (char *)s;
	c2 = (char)c;
	while (*str)
	{
		if (*str == c2)
			return (str);
		str++;
	}
	if (c == 0)
		return (str);
	return (0);
}
