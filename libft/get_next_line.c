#include "get_next_line.h"

void	*ft_strndup(char const *s, int begin, int end)
{
	char	*dest;
	int		i;
	int		j;

	i = begin;
	j = 0;
	dest = (char *) malloc(sizeof(char) * (end - begin + 2));
	if (!dest)
		return (NULL);
	while (i <= end)
	{
		dest[j] = s[i];
		j++;
		i++;
	}
	dest[j] = 0;
	return (dest);
}

t_list	*ft_lstnnew(void *s, int begin, int end)
{
	t_list	*l;

	l = (t_list *) malloc(sizeof(t_list));
	if (!l)
		return (NULL);
	l->content = ft_strndup((char *)s, begin, end);
	if (!(l->content))
	{
		free(l);
		return (NULL);
	}
	l->next = NULL;
	return (l);
}

void	ft_lstadd_back(t_list **alst, t_list *new)
{
	t_list	*cursor;

	if (alst && *alst)
	{
		cursor = *alst;
		while (cursor->next)
		{
			cursor = cursor->next;
		}
		cursor->next = new;
	}
	else if (alst)
		*alst = new;
}

int	ft_lstsize(t_list *lst)
{
	int		i;
	t_list	*cursor;

	i = 0;
	cursor = lst;
	while (cursor)
	{
		cursor = cursor->next;
		i++;
	}
	return (i);
}

void	ft_lstclear(t_list **lst, void (*del)(void *))
{
	t_list	*temp;

	if (!del || !lst ||!(*lst))
		return ;
	temp = *lst;
	while (*lst)
	{
		temp = (*lst)->next;
		del((*lst)->content);
		free(*lst);
		*lst = temp;
	}
}
void	clean_buffer(t_buf *reader)
{
	int	i;

	i = 0;
	while (i < reader->memo)
	{
		reader->buffer[i] = 0;
		i++;
	}
}

void	check_eol(int fd, t_count *count, t_buf *r, t_list **lst)
{
	if (r->memo < BUFFER_SIZE && r->buffer[r->memo] == '\n')
	{
		ft_lstadd_back(lst, ft_lstnnew(r->buffer, r->begin, r->memo));
		count->end = 1;
		r->begin = r->memo + 1;
		r->memo++;
	}
	else if (((r->memo < BUFFER_SIZE) && !(r->buffer[r->memo]))
		|| (r->memo == BUFFER_SIZE))
	{
		ft_lstadd_back(lst, ft_lstnnew(r->buffer, r->begin, r->memo));
		clean_buffer(r);
		if (!read(fd, r->buffer, BUFFER_SIZE))
			count->end = 1;
		r->memo = 0;
		r->begin = 0;
	}
}

char	*ft_makeline(t_list *lst, int n)
{
	size_t	i;
	char	*dest;
	t_list	*cursor;
	size_t	j;
	char	*cpy;

	dest = (char *) malloc((((ft_lstsize(lst)) * BUFFER_SIZE) + n + 2));
	if (!dest)
		return (NULL);
	i = 0;
	cursor = lst;
	j = 0;
	while (cursor)
	{
		j = 0;
		cpy = cursor->content;
		while (cpy[j])
			dest[i++] = cpy[j++];
		cursor = cursor->next;
	}
	dest[i] = '\0';
	return (dest);
}

char	*get_next_line(int fd)
{
	static t_buf	reader = {.buffer = "", .memo = 0, .begin = 0};
	char			*dest;
	t_count			count;
	t_list			*lst;

	count.end = 0;
	if (!(reader.buffer[0]) && (read(fd, reader.buffer, BUFFER_SIZE) <= 0))
		return (NULL);
	lst = NULL;
	while (!count.end)
	{
		while (reader.memo < BUFFER_SIZE
			&& reader.buffer[reader.memo] && reader.buffer[reader.memo] != '\n')
			reader.memo++;
		check_eol(fd, &count, &reader, &lst);
	}
	dest = ft_makeline(lst, reader.memo);
	ft_lstclear(&lst, &free);
	if (!dest || !(*dest))
	{
		free(dest);
		return (NULL);
	}
	return (dest);
}