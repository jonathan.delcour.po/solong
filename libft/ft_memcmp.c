/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 05:15:43 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/28 10:00:02 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int	ft_memcmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	i = -1;
	if (!n)
		return (0);
	while (++i < n - 1)
		if ((unsigned char)*(s1 + i) - (unsigned char)*(s2 + i))
			return ((unsigned char)*(s1 + i) - (unsigned char)*(s2 + i));
	return ((unsigned char)*(s1 + i) - (unsigned char)*(s2 + i));
}
