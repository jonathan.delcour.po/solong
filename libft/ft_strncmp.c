/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 17:23:23 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/26 18:05:00 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;

	if (!n)
		return (n);
	i = -1;
	while (++i < n - 1 && *(s1 + i) && *(s2 + i))
		if (*(s1 + i) - *(s2 + i))
			return ((unsigned char)*(s1 + i) - (unsigned char)*(s2 + i));
	return ((unsigned char)*(s1 + i) - (unsigned char)*(s2 + i));
}
