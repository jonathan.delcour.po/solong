/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 05:43:51 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/30 04:22:41 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

static int	ft_iswhite(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\f' || c == '\r'
		|| c == '\v');
}

int	ft_atoi(const char *nptr)
{
	size_t	i;
	int		res;
	int		sign;

	sign = 1;
	res = 0;
	i = 0;
	while (*(nptr + i) && ft_iswhite(*(nptr + i)))
		i++;
	if (*(nptr + i) == '-' || *(nptr + i) == '+')
		sign *= (i++, 1 - 2 * (*(nptr + i - 1) == '-'));
	while (*(nptr + i) && ft_isdigit(*(nptr + i)))
		res = (i++, res * 10 + (*(nptr + i - 1) - '0'));
	return (res * sign);
}
