/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 03:47:41 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/24 04:10:50 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

char	*ft_memchr(const void *s, int c, size_t n)
{
	char	c2;
	char	*str;
	size_t	i;

	i = 0;
	str = (char *)s;
	c2 = (char)c;
	while (i < n)
	{
		if (*(str + i) == c2)
			return (str + i);
		i++;
	}
	return (0);
}
