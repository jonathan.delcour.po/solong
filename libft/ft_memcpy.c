/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 20:55:15 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/24 04:09:22 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	char	*dest2;
	char	*src2;
	size_t	i;

	i = -1;
	dest2 = (char *)dest;
	src2 = (char *)src;
	if (!src && !dest)
		return (NULL);
	while (++i - n)
		i[dest2] = i[src2];
	return (dest);
}
