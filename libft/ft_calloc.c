/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 23:02:16 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/30 04:31:56 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

void	*ft_calloc(size_t nbmemb, size_t size)
{
	char	*result;
	size_t	i;

	i = -1;
	if (nbmemb == 0 || size == 0)
	{
		nbmemb = 1;
		size = 1;
	}
	result = (char *)malloc(nbmemb * size);
	if (!result)
		return ((void *)result);
	while (++i < nbmemb * size)
		i[result] = 0;
	return ((void *)result);
}
