/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/23 17:08:50 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 09:16:43 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	*str;
	int		len;
	char	c2;

	c2 = (char)c;
	len = ft_strlen(s);
	str = (char *)s;
	str += len;
	while (str >= s)
	{
		if (*str == c2)
			return (str);
		str--;
	}
	return (0);
}
