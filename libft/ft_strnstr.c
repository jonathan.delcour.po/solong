/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 05:21:48 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 09:16:19 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	c;
	size_t	k;

	if (ft_strlen(little) == 0)
		return ((char *)big);
	c = 0;
	k = 0;
	while (c < len && big[c])
	{
		while (c + k < len && big[c + k] && little[k]
			&& big[c + k] == little[k])
			k++;
		if (!little[k])
			return ((char *)big + c);
		c++;
		k = 0;
	}
	return (NULL);
}
