/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/22 21:37:47 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 08:53:21 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	size_t	i;
	char	*dest2;
	char	*src2;

	dest2 = (char *)dest;
	src2 = (char *)src;
	if (!dest)
		return (NULL);
	if (!src)
		return (dest);
	if (src < dest)
	{
		i = -1;
		while (++i < n)
			(n - i - 1)[dest2] = (n - i - 1)[src2];
	}
	else
	{
		i = -1;
		while (++i < n)
			i[dest2] = i[src2];
	}
	return (dest);
}
