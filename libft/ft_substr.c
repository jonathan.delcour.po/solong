/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 00:31:25 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 09:10:05 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>
#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*result;
	size_t	slen;

	slen = ft_strlen(s);
	if (start >= slen)
		return ((char *)ft_calloc(1, 1));
	if (start + len > slen)
		slen = slen - start;
	else
		slen = len;
	result = (char *)malloc(slen + 1);
	if (!result)
		return (result);
	ft_strlcpy (result, s + start, len + 1);
	return (result);
}
