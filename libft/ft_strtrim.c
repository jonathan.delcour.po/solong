/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 01:25:19 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/28 09:56:40 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

static int	ft_in(char c, char const *set)
{
	while (*set)
	{
		if (c == *set)
			return (1);
		set++;
	}
	return (0);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	size_t	start;
	size_t	end;
	size_t	len;

	len = ft_strlen(s1);
	start = 0;
	end = len -1;
	while (*(s1 + start) && ft_in(*(s1 + start), set))
		start++;
	while (end > 0 && ft_in(*(s1 + end), set))
		end--;
	return (ft_substr(s1, start, len - (len - end) - start + 1));
}
