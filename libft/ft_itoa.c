/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/28 04:31:18 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 10:09:24 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int	replen(int n)
{
	int	i;

	i = 0;
	if (n == 0)
		return (1);
	if (n < 0)
		i++;
	while (n != 0)
		n = (i++, n / 10);
	return (i);
}

char	*ft_itoa(int n)
{
	int		i;
	char	*result;

	i = replen(n);
	result = malloc(i + 1);
	if (!result)
		return (NULL);
	result[i] = '\0';
	result[0] = '0';
	if (n < 0)
		result[0] = '-';
	while (n != (--i, 0))
	{
		result[i] = (n % 10 * ((n % 10 > 0) - (n % 10 < 0))) + '0';
		n /= 10;
	}
	return (result);
}
