/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/27 22:59:39 by jdelcour          #+#    #+#             */
/*   Updated: 2021/12/01 12:01:48 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"
#include <stdlib.h>

static	size_t	countchain(char const *s, char c)
{
	size_t	result;

	result = 0;
	while (*s)
	{
		while (*s && *s == c)
			s++;
		if (*s)
			result++;
		while (*s && *s != c)
			s++;
	}
	return (result);
}

static void	*free_all(char **var, size_t i)
{
	if (i == 0)
		return (NULL);
	i--;
	while (i > 0)
	{
		free(var[i]);
		i--;
	}
	free(var);
	return (NULL);
}

char	**ft_split(char const *s, char c)
{
	size_t	nbchaine;
	size_t	i;
	size_t	curlen;
	char	**result;

	i = 0;
	nbchaine = countchain(s, c);
	result = (char **)malloc((nbchaine + 1) * sizeof(char *));
	if (!result)
		return (NULL);
	while (i < nbchaine)
	{
		curlen = 0;
		while (*s && *s == c)
			s++;
		while (*s && *s != c)
			curlen += (s++, 1);
		result[i] = malloc(curlen + 1);
		if (!result[i])
			return (free_all(result, i));
		ft_strlcpy(result[i], s - curlen, curlen + 1);
		i++;
	}
	result[i] = NULL;
	return (result);
}
