/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/25 01:02:05 by jdelcour          #+#    #+#             */
/*   Updated: 2021/11/30 06:56:29 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include <stdlib.h>
#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t	totallen;
	char	*result;

	totallen = ft_strlen(s1) + ft_strlen(s2);
	result = (char *)malloc(totallen + 1);
	if (!result)
		return (result);
	ft_strlcpy(result, s1, totallen + 1);
	ft_strlcat(result, s2, totallen + 1);
	return (result);
}
