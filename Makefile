CC			= clang
CFLAGS		= -Wall -Werror -Wextra -g3

DEPFLAGS	= -MP -MMD

NAME		= so_long
SRCS		= main.c map.c map2.c lst.c image.c move.c pos.c pos2.c
SDIR		= src

_OBJS		= $(SRCS:.c=.o)
OBJS		= $(patsubst %,$(ODIR)/%,$(_OBJS))
ODIR		= obj

HEADER		= libft/libft.h
INC			= -Ilibft -Imlx_linux
MLX_LIBS	= -L/usr/lib -lXext -lX11 -lm -lz

all: $(NAME)

libft/libft.a:
	make -C libft
mlx_linux/libmlx.a:
	make -C mlx_linux

$(NAME): $(OBJS) libft/libft.a mlx_linux/libmlx.a
	$(CC) $(CFLAGS) $(OBJS) -Llibft -lft -Ilibft $(MLX_LIBS) mlx_linux/libmlx.a -o $(NAME)

fclean: clean
	rm -rf so_long
	make fclean -C libft
	make clean -C mlx_linux

clean:
	rm -rf obj

$(ODIR)/%.o: $(SDIR)/%.c mlx_linux/libmlx.a
	mkdir -p $(ODIR)
	$(CC) $(CFLAGS) $(DEPFLAGS) $(INC) -c $< -o $@

-include $(SRCS:%.c=%.d)

re: fclean all

rel: re
	./$(NAME)

$(ODIR):
	mkdir $(ODIR)

.PHONY: re clean fclean all
