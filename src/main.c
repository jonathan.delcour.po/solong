/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jdelcour <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/11 02:15:55 by jdelcour          #+#    #+#             */
/*   Updated: 2022/08/11 03:37:18 by jdelcour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "mlx.h"
#include <sys/types.h>
#include <sys/stat.h>
#include "so_long.h"
#include "map.h"
#include "image.h"



void fill_win(t_game *game)
{
	unsigned int x;
	unsigned int y;
	char c;

	x = 0;
	y = 0;
	while (y < game->map->height )
	{
		while (x < game->map->lenght )
		{
			c = game->map->map[y][x];
			if (c == COLLECTIBLE)
				mlx_put_image_to_window(game->mlx, game->mlx_win, game->collectible_image, 50 * x, 50 * y);
			else if(c == EXIT)
				mlx_put_image_to_window(game->mlx, game->mlx_win, game->exit_image, 50 * x, 50 * y);
			else if(c == PLAYER)
				mlx_put_image_to_window(game->mlx, game->mlx_win, game->player_image, 50 * x, 50 * y);
			else if (c == VOID)
				mlx_put_image_to_window(game->mlx, game->mlx_win, game->void_image, 50 * x, 50 * y);
			else if (c == WALL)
				mlx_put_image_to_window(game->mlx, game->mlx_win,game->wall_image, 50 * x, 50 * y);
			x++;
		}
		y++;
		x = 0;
	}
}

int init_image(t_game *game)
{
	game->void_image = get_void_img(game->mlx);
	game->player_image = get_player_img(game->mlx);
	game->exit_image = get_exit_img(game->mlx);
	game->wall_image = get_wall_img(game->mlx);
	game->collectible_image = get_collectible_img(game->mlx);
	return (1);
}

t_game *init_game(char *map_path)
{
	t_game *game;
	(void)map_path;
	
	game = malloc(sizeof(game));
	game->map = init_map(map_path);
	game->mlx = mlx_init();
	game->mlx_win = mlx_new_window(game->mlx, game->map->lenght*50,	game->map->height*50, "j");
	game->move_count = 0;
	init_image(game);
	return (game);
}

int	key_hook(int keycode, t_game *game)
{
	if (keycode == KEY_W)
		movePlayer(game, 0, 1);
	else if (keycode == KEY_A)
		movePlayer(game, -1, 0);
	else if (keycode == KEY_S)
		movePlayer(game, 0, -1);
	else if (keycode == KEY_D)
		movePlayer(game, 1, 0);
	else if (keycode == 65307)
		printf("exit");
	return (0);
}

int	loop_hook(t_game *game)
{
	(void)game;
	return (0);
}


int	main(int argc, char *argv[])
{
	t_game *game;

    if (argc != 2)
		return (EXIT_FAILURE);
	printf("a\n");
    game = init_game(argv[1]);
	printf("ble\n");
	mlx_loop_hook(game->mlx, loop_hook, game);
	printf("c\n");
	mlx_key_hook(game->mlx_win, key_hook, game);
	printf("d\n");
	fill_win(game);
	printf("e\n");
	mlx_loop(game->mlx);
}
