#ifndef IMAGE_H
# define IMAGE_H
    void	*get_wall_img(void *mlx);
    void	*get_player_img(void *mlx);
    void	*get_exit_img(void *mlx);
    void	*get_void_img(void *mlx);
    void	*get_collectible_img(void *mlx);
#endif