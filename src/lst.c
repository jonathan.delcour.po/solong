#include <unistd.h>
#include <stdlib.h>
#include "lst.h"

t_lst *lst_create(void *content)
{
    t_lst *lst = malloc(sizeof(t_lst));
    if (!lst)
        return (NULL);
    lst->content = content;
    return lst;
}

int lst_add(t_lst *lst, void *content)
{
    t_lst *last;
    t_lst *new;

    last = lst;
    while (last->next)
        last = last->next;
    new = lst_create(content);
    if (!new)
        return (EXIT_FAILURE);
    last->next = new;
    return (EXIT_SUCCESS);
}

int lst_depth(t_lst *lst)
{
    t_lst   *curr;
    int     depth;

    if (!lst)
        return (-1);
    curr = lst;
    depth = 1;
    while (curr->next)
    {
        curr = curr->next;
        depth++;
    }
    return (depth);
}

