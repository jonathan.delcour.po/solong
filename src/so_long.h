#ifndef SOLONG_H
# define SOLONG_H
# define WALL '1'
# define COLLECTIBLE 'C'
# define EXIT 'E'
# define PLAYER 'P'
# define VOID '0'
# define NOT_IN_MAP '?'
# define KEY_W 119
# define KEY_A 97
# define KEY_S 115
# define KEY_D 100
typedef struct s_map{
    char **map;
    unsigned int height;
    unsigned int lenght;
    unsigned int colectible;
}   t_map;
typedef struct	s_game {
	void	        *void_image;
    void	        *player_image;
    void	        *exit_image;
    void	        *wall_image;
    void	        *collectible_image;
	void	        *mlx;
	void	        *mlx_win;
	t_map	        *map;
	int		        keycode;
    unsigned int    move_count;
}				    t_game;
typedef struct s_pos
{
    int x;
    int y;
} t_pos;
#include "map.h"
#endif