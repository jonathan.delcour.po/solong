#ifndef LST_H
# define LST_H
typedef struct s_lst t_lst;
struct s_lst
{
    void *content;
    t_lst *next;
};

t_lst *lst_create(void *content);
int lst_add(t_lst *lst, void *content);
int lst_depth(t_lst *lst);
#endif