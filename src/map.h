#ifndef MAP_H
# define MAP_H
# include "lst.h"
# include "so_long.h"

t_lst *init_lst_map(char *path);
t_map *init_map(char *path);
void free_map(t_map *map);

t_pos pos_left(t_pos pos);
t_pos pos_right(t_pos pos);
t_pos pos_up(t_pos pos);
t_pos pos_down(t_pos pos);
t_pos pos_null();
t_pos pos_add(t_pos pos, int x, int y);
char    pos_get_content(t_map *map, t_pos pos);

t_pos getPlayerPos(t_map *map);
int is_pos_in_map(t_map *map, t_pos pos);
int get_collectible_count(t_map *map);
void map_set_content(t_game *game, t_pos pos, char content);
int movePlayer(t_game *game, int x, int y);
void count_move(t_game *game);
#endif