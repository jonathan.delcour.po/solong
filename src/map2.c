#include "map.h"
#include "so_long.h"
#include "mlx.h"
#include "image.h"

t_pos getPlayerPos(t_map *map)
{
    unsigned int x;
	unsigned int y;
    t_pos pos;

	x = 0;
	y = 0;
	while (y < map->height)
	{
		while (x < map->lenght)
		{
			x++;
            if (map->map[y][x] == PLAYER)
            {
                pos.x = x;
                pos.y = y;
                return (pos);
            }
		}
		y++;
		x = 0;
	}
	return (pos_null());
}

int get_collectible_count(t_map *map)
{
    unsigned int x;
	unsigned int y;
	int count;

	count = 0;
	x = 0;
	y = 0;
	while (y < map->height)
	{
		while (x < map->lenght)
		{
			x++;
            if (map->map[y][x] == COLLECTIBLE)
                count++;
		}
		y++;
		x = 0;
	}
	return (count);
}

void map_set_content(t_game *game, t_pos pos, char content)
{
	game->map->map[pos.y][pos.x] = content;
	if(game->mlx)
	{
		if (content == COLLECTIBLE)
			mlx_put_image_to_window(game->mlx, game->mlx_win, game->collectible_image, 50 * pos.x, 50 * pos.y);
		else if(content == EXIT)
			mlx_put_image_to_window(game->mlx, game->mlx_win, game->exit_image, 50 * pos.x, 50 * pos.y);
		else if(content == PLAYER)
			mlx_put_image_to_window(game->mlx, game->mlx_win, game->player_image, 50 * pos.x, 50 * pos.y);
		else if (content == VOID)
			mlx_put_image_to_window(game->mlx, game->mlx_win, game->void_image, 50 * pos.x, 50 * pos.y);
		else if (content == WALL)
			mlx_put_image_to_window(game->mlx, game->mlx_win,game->wall_image, 50 * pos.x, 50 * pos.y);
	}
}
