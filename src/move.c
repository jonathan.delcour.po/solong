#include "map.h"
#include "so_long.h"
#include "libft.h"
#include "mlx.h"

void exit_game(t_game *game)
{
    free_map(game->map);
	mlx_destroy_display(game->mlx);
	free(game->mlx);
	free(game);
	exit(0) ;
}

int movePlayer(t_game *game, int x, int y)
{
    t_pos player_pos;
    t_pos pos_to_go;
    char pos_to_go_content;

    player_pos = getPlayerPos(game->map);
    pos_to_go = pos_add(player_pos, x, y);
    pos_to_go_content = pos_get_content(game->map, pos_to_go);
    if (pos_to_go_content == NOT_IN_MAP || pos_to_go_content == WALL)
        return (0);
    else if (pos_to_go_content == VOID || pos_to_go_content == PLAYER || pos_to_go_content == COLLECTIBLE)
    {
        map_set_content(game, player_pos, VOID);
        map_set_content(game, pos_to_go, PLAYER);
        count_move(game);
        return (1);
    }
    else if (pos_to_go_content == EXIT)
    {
        if (get_collectible_count(game->map) == 0)
        {
            count_move(game);
            exit_game(game);
        }
        return (1);
    }
    return (0);
}

void count_move(t_game *game)
{
    game->move_count++;
    ft_putstr_fd("Move count:", 1);
    ft_putnbr_fd(game->move_count, 1);
    ft_putchar_fd('\n', 1);
}