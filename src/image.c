#include "mlx.h"

void	*get_wall_img(void *mlx)
{
	int width;
	int height;

	width = 50;
	height = 50;
	return mlx_xpm_file_to_image(mlx, "img/wall.xpm", &width, &height);
}

void	*get_player_img(void *mlx)
{
	int width;
	int height;

	width = 50;
	height = 50;
	return mlx_xpm_file_to_image(mlx, "img/player.xpm", &width, &height);
}

void	*get_exit_img(void *mlx)
{
	int width;
	int height;

	width = 50;
	height = 50;
	return mlx_xpm_file_to_image(mlx, "img/exit.xpm", &width, &height);
}

void	*get_void_img(void *mlx)
{
	int width;
	int height;

	width = 50;
	height = 50;
	return mlx_xpm_file_to_image(mlx, "img/void.xpm", &width, &height);
}

void	*get_collectible_img(void *mlx)
{
	int width;
	int height;

	width = 50;
	height = 50;
	return mlx_xpm_file_to_image(mlx, "img/collectible.xpm", &width, &height);
}