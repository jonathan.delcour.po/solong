#include "map.h"
#include <unistd.h>
#include <stddef.h>
#include <stdio.h>
#include <fcntl.h>
#include "so_long.h"
#include "libft.h"

t_lst *init_lst_map(char *path)
{

	char	*line;
	t_lst	*lst_map;
	int		fd;

	fd = open(path, O_RDONLY);
	if (fd < 0)
		return (NULL);
	line = get_next_line(fd);
	if (!line)
		return (NULL);
	if (line[ft_strlen(line) - 1] == '\n')
		line[ft_strlen(line) - 1] = '\0';
	lst_map = lst_create(line);
	while (line)
	{
		line = get_next_line(fd);
		if (line)
		{
			lst_add(lst_map, line);
			if (line[ft_strlen(line) - 1] == '\n')
				line[ft_strlen(line) - 1] = '\0';
		}
	}
	close(fd);
	return lst_map;
}

int	lst_map_lenght(t_lst *lst_map)
{
	size_t	len;
	t_lst	*curr;

	curr = lst_map;
	len = ft_strlen(curr->content);
	while (curr->next)
	{
		if (ft_strlen(curr->content) != len)
			return (-1);
		curr = curr->next;
	}
	return (len);
}

int fill_map(t_lst *lst_map, t_map *map)
{
	t_lst *curr;
	int i;

	i = 0;
	curr = lst_map;
	map->map = malloc(sizeof(char*) * (map->height + 1));
	if (!map->map)
	{
		return (-1);
	}
	while(curr->next)
	{
		map->map[i] = curr->content;
		curr = curr->next;
		i++;
	}
	map->map[i] = curr->content;
	return (1);
}

t_map *lst_map_to_map(t_lst *lst_map)
{
	t_map *map;
	int len;

	len = lst_map_lenght(lst_map);
	if(len == -1)
		return (NULL);
	map = malloc(sizeof(map));
	if (!map)
		return (NULL);
	map->height	= lst_depth(lst_map);
	map->lenght = len;
	fill_map(lst_map, map);
	map->colectible = 0;
	return (map);
}

void free_lst_map(t_lst *map)
{
    t_lst *curr;
    t_lst *tmp;
    curr = map;
    while(curr->next)
    {
        tmp = curr;
        curr = curr->next;
        free(tmp);
    }
    free(curr);
}

void free_lst_map_and_content(t_lst *map)
{
    t_lst *curr;
    t_lst *tmp;
    curr = map;
    while(curr->next)
    {
        tmp = curr;
        curr = curr->next;
		free(tmp->content);
        free(tmp);
    }
	free(curr->content);
    free(curr);
}

int is_map_close(t_map *map)
{
	size_t i;

	i = 0;
	while(i < map->lenght)
	{
		if (map->map[0][i] != WALL)
			return (0);
		i++;
	}
	i = 0;
	while(i < map->lenght)
	{
		if (map->map[map->height - 1][i] != WALL)
			return (0);
		i++;
	}
	i = 0;
	while(i < map->height)
	{
		if (map->map[i][0] != WALL)
			return (0);
		i++;
	}
	i = 0;
	while(i < map->height)
	{
		if (map->map[i][map->lenght - 1] != WALL)
			return (0);
		i++;
	}
	return (1);
}

int is_map_containing_required_entry(t_map *map)
{
	unsigned int x;
	unsigned int y;
	char c;
	int e;
	int p;

	x = 0;
	y = 0;
	e = 0;
	p = 0;
	map->colectible = 0;

	while (y < map->height)
	{
		while (x < map->lenght)
		{
			c = map->map[y][x];
			if (c == COLLECTIBLE)
				map->colectible++;
			else if(c == EXIT)
				e++;
			else if(c == PLAYER)
				p++;
			else if (c == VOID || c == WALL)
				;
			else
			{
				printf("mdr: %c\n", c);
				return (0);
			}
			x++;
		}
		y++;
		x = 0;
	}
	return (!(p == 0 || e == 0 || map->colectible == 0));
}

int is_map_valid(t_map *map)
{
	int res;

	res = 1;
	printf("res: a : %d\n", res);
	res = res && is_map_close(map);
	printf("res: b : %d\n", res);
	res = res && is_map_containing_required_entry(map);
	printf("res: c : %d\n", res);
	return (res); 
}

void free_map(t_map *map)
{
	unsigned int i;

	i = 0;
	while (i < map->height)
	{
		free(map->map[i]);
		i++;
	}
	free(map->map);
	free(map);
}

t_map *init_map(char *path)
{
    t_map	*map;
	t_lst	*lst_map;

	lst_map = init_lst_map(path);
	if (!lst_map)
		return (NULL);
	map = lst_map_to_map(lst_map);
	if (!map)
	{
		free_lst_map_and_content(lst_map);
		return (NULL);
	}
	free_lst_map(lst_map);
	if(!is_map_valid(map))
	{
		free_map(map);
		return (NULL);
	}
    return map;
}

