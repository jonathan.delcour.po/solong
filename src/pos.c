#include "map.h"

t_pos pos_left(t_pos pos)
{
    pos.x--;
    return pos;
}

t_pos pos_right(t_pos pos)
{
    pos.x++;
    return pos;
}

t_pos pos_up(t_pos pos)
{
    pos.y++;
    return pos;
}

t_pos pos_down(t_pos pos)
{
    pos.y++;
    return pos;
}

t_pos pos_null()
{
    t_pos pos;

    pos.x = -1;
    pos.y = -1;
    return (pos);
}