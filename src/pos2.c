#include "map.h"
#include "so_long.h"
int is_pos_in_map(t_map *map, t_pos pos)
{
    return (pos.x < (int) map->lenght && pos.x >= 0 && pos.y < (int) map->height && pos.y > 0);
}

t_pos pos_add(t_pos pos, int x, int y)
{
    pos.x += x;
    pos.y += y;
    return (pos);
}

char    pos_get_content(t_map *map, t_pos pos)
{
    if (is_pos_in_map(map, pos))
        return (map->map[pos.x][pos.y]);
    return (NOT_IN_MAP);
}